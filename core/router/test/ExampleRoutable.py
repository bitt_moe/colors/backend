from fastapi.responses import PlainTextResponse

from core.router import get, post, Routable


class ExampleRoutable(Routable):
    def __init__(self, injected: int) -> None:
        super().__init__()
        self.__injected = injected

    @get(path='/add/{x}')
    def add(self, x: int) -> int:
        return x + self.__injected

    @post(path='/sub/{x}')
    def sub(self, x: int) -> int:
        return x - self.__injected

    @get(path='/async')
    async def do_async(self) -> int:
        return self.__injected + 1

    @get(path='/aecho/{val}', response_class=PlainTextResponse)
    async def aecho(self, val: str) -> str:
        return f'{val} {self.__injected}'
