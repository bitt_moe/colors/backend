import dataclasses

from functools import partial
from typing import Any, Callable, List, TypeVar

from fastapi.routing import APIRouter

from .EndpointDefinition import EndpointDefinition
from .RoutableMeta import RoutableMeta
from ..decorator.copy_doc import copy_doc

AnyCallable = TypeVar('AnyCallable', bound=Callable[..., Any])


class Routable(metaclass=RoutableMeta):
    """Base class for all classes the want class-based routing.

    This Uses RoutableMeta as a metaclass and various decorators like @get or @post from the decorators' module. The
    decorators just mark a method as an endpoint. The RoutableMeta then converts those to a list of desired endpoints in
    the _endpoints class method during class creation. The constructor constructs an APIRouter and adds all the routes
    in the _endpoints to it, so they can be added to an app via FastAPI.include_router or similar.
    """

    _endpoints: List[EndpointDefinition] = []

    def __init__(self, *args, **kwargs) -> None:
        self.router = APIRouter(*args, **kwargs)
        for endpoint in self._endpoints:
            endpoint_func = partial(endpoint.endpoint, self)
            self.router.add_api_route(
                endpoint=copy_doc(endpoint.endpoint)(endpoint_func),
                **dataclasses.asdict(endpoint.args)
            )
