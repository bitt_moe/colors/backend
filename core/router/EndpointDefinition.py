from typing import (Any, Callable)
from dataclasses import dataclass

from .RouteArgs import RouteArgs


@dataclass
class EndpointDefinition:
    """RouteArgs plus the endpoint.

    Endpoint is separate as it has to be bound to self before it can be used.
    """
    endpoint: Callable[..., Any]
    args: RouteArgs
