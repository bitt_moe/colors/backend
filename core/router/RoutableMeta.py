import inspect
from typing import Any, Dict, List, Tuple, Type, cast

from .EndpointDefinition import EndpointDefinition


class RoutableMeta(type):
    """This is a metaclass that converts all the methods that were marked by a route/path decorator into values on a
    class member called _endpoints that the Routable constructor then uses to add the endpoints to its router."""
    def __new__(mcs: Type[type], name: str, bases: Tuple[Type[Any]], attrs: Dict[str, Any]) -> 'RoutableMeta':
        endpoints: List[EndpointDefinition] = []
        for v in attrs.values():
            if inspect.isfunction(v) and hasattr(v, '_endpoint'):
                endpoints.append(v._endpoint)
        attrs['_endpoints'] = endpoints
        return cast(RoutableMeta, type.__new__(mcs, name, bases, attrs))
