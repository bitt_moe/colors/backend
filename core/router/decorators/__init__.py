import inspect
from typing import (Any, Callable, List, TypeVar)

from ..EndpointDefinition import EndpointDefinition
from ..RouteArgs import RouteArgs

AnyCallable = TypeVar('AnyCallable', bound=Callable[..., Any])


def route(path: str, methods: List[str], **kwargs: Any) -> Callable[[AnyCallable], AnyCallable]:
    """
    General purpose route definition. Requires you to pass an array of HTTP methods like GET, POST, PUT, etc.

    The remaining kwargs are exactly the same as for FastAPI's decorators like @get, @post, etc.

    Most users will probably want to use the shorter decorators like @get, @post, @put, etc. so they don't have to pass
    the list of methods.
    """
    def marker(method: AnyCallable) -> AnyCallable:
        args = RouteArgs(path=path, methods=methods, **kwargs)
        if args.name is None:
            args.name = method.__name__
        setattr(method, '_endpoint', EndpointDefinition(endpoint=method, args=args))
        return method
    return marker
