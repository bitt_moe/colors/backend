# Source code from https://gitlab.com/companionlabs-opensource/classy-fastapi/

from .decorators.get import get
from .decorators.post import post
from .decorators.patch import patch
from .decorators.put import put
from .decorators.delete import delete
from .Routable import Routable

__all__ = ['Routable', 'get', 'post', 'patch', 'put', 'delete']
