start-dev:
	venv/bin/uvicorn main:app --reload

build-docker:
	docker build -t mcpe-server-manager:latest -f deploy/dev/Dockerfile .
