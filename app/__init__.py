from pathlib import Path
from loguru import logger


def get_root_folder(project_root_folder_name="app") -> Path:
    path = Path().absolute()
    for parent in path.parents:
        if parent.name == project_root_folder_name:
            return parent.parent
    return path


ROOT_FOLDER = get_root_folder()

logger.add(str(ROOT_FOLDER / "logs" / "application-1.log"), format="{time:YYYY-MM-DD at HH:mm:ss} | {level} | {message}", rotation="10 MB")
