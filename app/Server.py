from enum import Enum

from starlette.middleware.trustedhost import TrustedHostMiddleware

from core.App import App
from core.AppConfig import AppConfig
from app.routes import ServerRoutes
from app.ApplicationConfig import ApplicationConfig


class Server(App):

    def __init__(self):
        super().__init__()
        self.routers: id(Enum) = ServerRoutes
        self.server_config = ApplicationConfig()
        self.config = self._configure()
        self.debug: bool = False
        self.add_middleware(TrustedHostMiddleware, allowed_hosts=["*"])

    def _configure(self) -> AppConfig:
        app = AppConfig()
        app.title = self.server_config.application_title
        app.version = self.server_config.application_version
        app.mount_path = self.server_config.application_mount_path
        app.servers = []
        return app

