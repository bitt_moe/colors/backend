from app import ROOT_FOLDER

from dotenv import dotenv_values
from pathlib import Path


class ApplicationConfig:
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(ApplicationConfig, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self.ENV_FILENAME = ".env"
        self.env_file_path = ROOT_FOLDER / self.ENV_FILENAME
        self.__env = dotenv_values(self.env_file_path)

    @property
    def project_root_folder(self) -> Path:
        return Path("..").absolute()

    @property
    def debug(self) -> bool:
        raw_string = self.__env["APP_DEBUG"]
        return raw_string.lower() == "true"

    @property
    def public_key_path(self) -> Path:
        public_key_raw_path = self.__env["JWT_PUBLIC_KEY_PATH"]
        public_key_path = self.project_root_folder / public_key_raw_path
        if Path.exists(public_key_path):
            return public_key_path
        else:
            raise FileNotFoundError("Не найден файл открытого ключа")

    @property
    def application_title(self) -> str:
        return self.__env["APP_TITLE"]

    @property
    def application_version(self) -> str:
        return self.__env["APP_VERSION"]

    @property
    def application_mount_path(self) -> str:
        return self.__env["APP_MOUNT_PATH"]

    @property
    def application_allowed_hosts(self) -> list[str]:
        raw_string = self.__env["APP_ALLOWED_HOSTS"]
        return raw_string.replace(" ", "").split(",")

    @property
    def jwt_allowed_issuer_hosts(self) -> list[str]:
        raw_string = self.__env["JWT_ALLOWED_ISSUER_HOSTS"]
        return raw_string.replace(" ", "").split(",")

    def get_or_none(self, property_name: str) -> str or None:
        try:
            return self.__env[property_name] if self.__env[property_name] != "" else None
        except KeyError:
            return None

